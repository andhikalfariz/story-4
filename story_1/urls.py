from . import views
from django.urls import path

app_name = 'story1'

urlpatterns = [
    path('', views.index, name="index")
]